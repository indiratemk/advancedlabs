package com.example.simplevideoview;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

//    private static final String VIDEO_SAMPLE =
//            "https://developers.google.com/training/images/tacoma_narrows.mp4";
    private static final String VIDEO_SAMPLE = "tacoma_narrows";
    private static final String PLAYBACK_TIME = "play_time";
    private VideoView mVideoView;
    private int mCurrentPosition = 0;
    private TextView mBufferingTextView;
    private Button playButton;
    private Button skipButton;
    private boolean isPlaying = true;
    private boolean isFirstTime = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(PLAYBACK_TIME);
        }


        mVideoView = findViewById(R.id.videoview);

        mBufferingTextView = findViewById(R.id.buffering_textview);

        playButton = findViewById(R.id.switch_button);

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFirstTime) {
                    initializePlayer();
                    playButton.setText("Pause");
                    isFirstTime = false;
                    return;
                }
                if (isPlaying) {
                    playButton.setText("Play");
                    mCurrentPosition = mVideoView.getCurrentPosition();
                    releasePlayer();
                    isPlaying = false;
                } else {
                    playButton.setText("Pause");
                    initializePlayer();
                    isPlaying = true;
                }
            }
        });

        skipButton = findViewById(R.id.skip_button);

        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: " + mVideoView.getCurrentPosition());
                mCurrentPosition = mVideoView.getCurrentPosition() + 10000;
                initializePlayer();
            }
        });

//        MediaController controller = new MediaController(this);
//        controller.setMediaPlayer(mVideoView);
//        mVideoView.setMediaController(controller);

    }

    private Uri getMedia(String mediaName) {
        if (URLUtil.isValidUrl(mediaName)) {
            return Uri.parse(mediaName);
        }
        return Uri.parse("android.resource://" + getPackageName()
                + "/raw/" + mediaName);
    }

    private void initializePlayer() {
        mBufferingTextView.setVisibility(VideoView.VISIBLE);
        Uri videoUri = getMedia(VIDEO_SAMPLE);
        mVideoView.setVideoURI(videoUri);
        if (mCurrentPosition > 0) {
            mVideoView.seekTo(mCurrentPosition);
        } else {
            mVideoView.seekTo(1);
        }
        mVideoView.start();
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Toast.makeText(MainActivity.this, "Playback completed",
                        Toast.LENGTH_SHORT).show();
                mVideoView.seekTo(1);
            }
        });

        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mBufferingTextView.setVisibility(VideoView.INVISIBLE);
                if (mCurrentPosition > 0) {
                    mVideoView.seekTo(mCurrentPosition);
                } else {
                    mVideoView.seekTo(1);
                }

                mVideoView.start();
            }
        });
    }

    private void releasePlayer() {
        mVideoView.stopPlayback();
    }

    @Override
    protected void onStart() {
        super.onStart();
//        initializePlayer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        releasePlayer();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            mVideoView.pause();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(PLAYBACK_TIME, mVideoView.getCurrentPosition());
    }
}
