package com.example.testwidget;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private static final String mSharedPrefFile =
            "com.example.android.testwidget";
    private static final String EDIT_TEXT = "edit_text";

    private EditText mEditText;
    private Button mButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final SharedPreferences prefs = getSharedPreferences(
                mSharedPrefFile, 0);

        mEditText = findViewById(R.id.typed_text);
        mButton = findViewById(R.id.send_button);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor prefEditor = prefs.edit();
                prefEditor.putString(EDIT_TEXT, mEditText.getText().toString());
                prefEditor.apply();
            }
        });

    }
}
