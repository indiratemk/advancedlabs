package com.example.propertyanimation;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

public class ChallengeActivity extends AppCompatActivity {

    private TextView mTextView;
    private ImageView cardView;

    private boolean isFrontSide = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge);

        mTextView = findViewById(R.id.animate_text);
        final float startSize = 0;
        final float endSize = 42;
        long animationDuration = 1000;

        ValueAnimator animator = ValueAnimator.ofFloat(startSize, endSize);
        animator.setDuration(animationDuration);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatedValue = (float) valueAnimator.getAnimatedValue();
                mTextView.setTextSize(animatedValue);
            }
        });

        animator.start();


        cardView = findViewById(R.id.card_view1);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(cardView, "scaleX", 1f, 0f);
                final ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(cardView, "scaleX", 0f, 1f);
                objectAnimator1.setInterpolator(new DecelerateInterpolator());
                objectAnimator2.setInterpolator(new AccelerateDecelerateInterpolator());
                objectAnimator1.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        if (isFrontSide) {
                            cardView.setImageResource(R.drawable.card2);
                            isFrontSide = false;
                        } else {
                            cardView.setImageResource(R.drawable.card);
                            isFrontSide = true;
                        }
                        objectAnimator2.start();
                    }
                });
                objectAnimator1.start();
            }
        });
    }
}
