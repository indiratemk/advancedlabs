package com.example.propertyanimation;

import android.content.Context;
import android.support.animation.DynamicAnimation;
import android.support.animation.FlingAnimation;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class FlingButton extends AppCompatButton {
    public FlingButton(Context context) {
        super(context);
    }

    public FlingButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FlingButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            FlingAnimation fling = new FlingAnimation(this, DynamicAnimation.ROTATION_X);
            fling.setStartVelocity(150)
                    .setMinValue(0)
                    .setMaxValue(1000)
                    .setFriction(0.1f)
                    .start();
        }

        return super.onTouchEvent(event);
    }
}
