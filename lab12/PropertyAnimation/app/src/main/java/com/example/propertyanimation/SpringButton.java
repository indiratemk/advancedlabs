package com.example.propertyanimation;

import android.content.Context;
import android.support.animation.DynamicAnimation;
import android.support.animation.SpringAnimation;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.view.MotionEvent;

import static android.support.animation.SpringForce.STIFFNESS_LOW;

public class SpringButton extends AppCompatButton {
    public SpringButton(Context context) {
        super(context);
    }

    public SpringButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpringButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            final SpringAnimation anim = new SpringAnimation(
                    this, DynamicAnimation.Y, 10)
                    .setStartVelocity(1000);
            anim.getSpring().setStiffness(STIFFNESS_LOW);
            anim.start();
        }
        return super.onTouchEvent(event);
    }
}
