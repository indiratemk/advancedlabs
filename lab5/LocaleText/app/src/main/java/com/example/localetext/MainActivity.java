package com.example.localetext;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.TimeUnit;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private NumberFormat mNumberFormat = NumberFormat.getInstance();
    private int mInputQuantity = 1;

    private TextView expirationDateView;
    private EditText enteredQuantity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showHelp();
            }
        });

        expirationDateView = findViewById(R.id.date_value);
        enteredQuantity = findViewById(R.id.currency_edit_text);

        final Date myDate = new Date();
        final long expirationDate = myDate.getTime() +
                TimeUnit.DAYS.toMillis(5);
        myDate.setTime(expirationDate);
        String myFormattedDate =
                DateFormat.getDateInstance().format(myDate);
        expirationDateView.setText(myFormattedDate);


//        Get device location country
//        Locale.getDefault().getCountry();

        enteredQuantity.setOnEditorActionListener
                (new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId,
                                                  KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            // Close the keyboard.
                            InputMethodManager imm = (InputMethodManager)
                                    v.getContext().getSystemService
                                            (Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                            // Parse string in view v to a number.
                            try {
                                // Use the number format for the locale.
                                mInputQuantity = mNumberFormat.parse(v.getText()
                                        .toString()).intValue();
                                v.setError(null);
                            } catch (ParseException e) {
                                Log.e(TAG, Log.getStackTraceString(e));
                                return false;
                            }
                            String myFormattedQuantity =
                                    mNumberFormat.format(mInputQuantity);
                            v.setText(myFormattedQuantity);

                            return true;
                        }
                        return false;
                    }
                });

    }


    private void showHelp() {
        // Create the intent.
        Intent helpIntent = new Intent(this, HelpActivity.class);
        // Start the HelpActivity.
        startActivity(helpIntent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle options menu item clicks here.
        switch (item.getItemId()) {
            case R.id.action_help:
                showHelp();
                return true;
            case R.id.action_language:
                Intent languageIntent = new
                        Intent(Settings.ACTION_LOCALE_SETTINGS);
                startActivity(languageIntent);
                return true;
            default:

        }
        return super.onOptionsItemSelected(item);
    }
}